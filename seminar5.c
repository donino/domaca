#include<stdlib.h>

typedef struct{
    float x, y;
    float r;
}CIRCLE;

CIRCLE *new_circle(float x, float y, float r)
{
    CIRCLE *s;
    
    s = malloc(sizeof(CIRCLE));
	if(s>0) // osetrenie, ardresa je vzdy kladna
	{
		s->x = x;
    		s->y = y;
    		s->r = r;
	}
    
    
    return s;
}

void destroy_circle(CIRCLE *to_destroy)
{
    free(to_destroy);
}

int main(void)
{
 	CIRCLE *kruznica;
 
 	kruznica=new_circle(1, 1, 5);
 
 	destroy_circle(kruznica);
 
 	return 0;
    
    
    
}
